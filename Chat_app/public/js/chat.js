
//メンバーのチャットを選択イベント
$("[id=hover]").change(function () {
    //取得件数初期化
    list_number = list_number_first;
    if ($("#more_list_div").hasClass("not-disp")) {
        $("#more_list_div").removeClass("not-disp");
    }
    //前にチェックしていたボタン
    if (checked_button != null) {
        var past = checked_button;
        var past_id = past.attr("id");
        var past_id_num = past_id.replace(/[^0-9]/g, '');
    }
    // $("#list_" + past_id_num).toggleClass("not-disp");
    // $("#input_field_" + past_id_num).toggleClass("not-disp");
    //今チェックしたボタン
    var radio_button = $(this).find("[name=member]");
    var code = radio_button.data('code');
    set_data(code);
    radio_button.prop('checked', true);
    $("#list_0").removeClass("not-disp");
    $("#input_field_0").removeClass("not-disp");
    //チェックボタンデータ更新
    checked_button = radio_button;
})
//個人かグループか
$('input[name="type"]').change(function () {
    //取得件数初期化
    list_number = list_number_first;
    var type_id = $(this).attr("id");
    //切り替えを行う場合一旦トーク画面を非表示に。
    if (checked_button != null) {
        var past = checked_button;
        past.prop('checked', false);
        var message_el = $("#list_0");
        var input_el = $("#input_field_0");
        if (!message_el.hasClass("not-disp")) {
            message_el.addClass("not-disp");
        }
        if (!input_el.hasClass("not-disp")) {
            input_el.addClass("not-disp");
        }
    }
    console.log(type_id);
    var type_group = $("#chat_group");
    var type_member = $("#chat_member");
    //個人とグループの切り替え
    if (type_id == "type_group") {
        chat_type = "chat_group";
        if (type_group.hasClass("not-disp")) {
            type_group.removeClass("not-disp");
        } else { }
        if (type_member.hasClass("not-disp")) { }
        else {
            type_member.addClass("not-disp");
        }
    } else if (type_id == "type_member") {
        chat_type = "chat_member";
        if (type_group.hasClass("not-disp")) { }
        else {
            type_group.addClass("not-disp");
        }
        if (type_member.hasClass("not-disp")) {
            type_member.removeClass("not-disp");
        } else { }
    }
})
//コメント送信ボタンを押したら
$("[id=send_message]").click(function () {
    var message = $("#api_message").val();
    $("#api_message").val("");
    if (message == "") {
        alert("入力されていません。");
        return;
    }
    console.log(list_code);
    var url = set_url("message");
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
    });
    $.ajax({
        //POST通信
        type: "post",
        url: url,
        dataType: "json",
        data: {
            comment: message,
            chat_list_code: list_code,
        },
    }).done(function () {
        list_number += 1;
        set_data(list_code);
        //alert("送信しました。");
    }).fail(function () {
        alert("通信に失敗しました。");
    });
})
//ファイルアップロードボタンをおしたら
$("[id=send_upload]").click(function () {
    var api_file = $("#api_file");
    var file_data = api_file.prop('files')[0];
    console.log(file_data);
    if (api_file.val() == "") {
        alert("ファイルが選択されていません。");
        return;
    }
    api_file.val("");
    var form = new FormData();
    form.append("file", file_data);
    form.append("chat_list_code", list_code);
    var url = set_url("file");
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            //'Content-Type': 'multipart/form-data'
        },
    });
    $.ajax({
        //POST通信
        type: "post",
        enctype: 'multipart/form-data',
        url: url,
        dataType: "json",
        data: form,
        processData: false,
        contentType: false,
    }).done(function () {
        list_number += 1;
        set_data(list_code);
        //alert("送信しました。");
    }).fail(function () {
        alert("通信に失敗しました。");
    });
})
//「さらに」で取得件数を増やす
$("#more_list").click(function () {
    list_number += list_number_plus;
    set_data(list_code);
})
//APIのurlセット
function set_url(type) {
    var url = "";
    switch (type) {
        case "chat_data":
            if (chat_type == "chat_group") {
                url = "/api/chatroom/" + list_code + "/group/get/" + list_number;
            } else {
                url = "/api/chatroom/" + list_code + "/get/" + list_number;
            }
            break;

        case "message":
            if (chat_type == "chat_group") {
                url = "/api/chatroom/group/create";
            } else {
                url = "/api/chatroom/create";
            }
            break;

        case "file":
            if (chat_type == "chat_group") {
                url = "/api/chatroom/group/file/upload";
            } else {
                url = "/api/chatroom/file/upload";
            }
            break;
    }
    return url;
}
//個人フォームチェック
$('#individual_form').submit(function () {
    //宛先
    if ($('[name=user_code_radio]:checked').prop('checked')) {
    } else {
        alert("宛先を1人選択してください。");
        return false;
    }
    var message = $("#form_message").val();
    var api_file = $("#formFile");
    if (api_file.val() == "" && message == "") {
        alert("メッセージ入力またはファイル選択をしてください。");
        return false;
    }
    return true;
});
//グループフォームチェック
$('#group_form').submit(function () {
    //グループ名
    var group_name_el = $("#group_name_id");
    if (group_name_el.val() == "") {
        alert("グループ名を入力してください。");
        return false;
    }
    //宛先
    if ($("input[data-check='check_box']:checked").size() < 1) {
        alert("グループメンバーを1人以上選択してください。");
        return false;
    }
    var message = $("#form_group_message").val();
    var api_file = $("#form_group_File");
    if (api_file.val() == "" && message == "") {
        alert("メッセージ入力またはファイル選択をしてください。");
        return false;
    }
    return true;
});


//チャットデータAPI
function set_data(id) {
    $("#message_list").addClass("not-disp");
    $("#loading").removeClass("not-disp");
    list_code = id;
    var is_read_string = "";
    var url = set_url("chat_data");
    $.ajax({
        type: "get",
        url: url,
        dataType: "json",
        success: data => {

            $("#list").empty();
            console.log(data.number_flag);
            if (data.number_flag) {
                $("#more_list_div").addClass("not-disp");
            }
            for (var i = 0; i < data.chat_data.length; i++) {
                //既読チェック
                if (chat_type == "chat_member") {
                    if (data.chat_data[i].read_type == 2) {
                        is_read_string = "既読";
                    } else {
                        is_read_string = "";
                    }
                }
                //自分側
                if (data.chat_data[i].sender_user_code == data.user.user_code) {
                    //コメント
                    if (data.chat_data[i].comment_type == 1) {
                        var html = `
                      <div
                          class="d-flex justify-content-between align-items-center w-100">
                          <strong></strong>
                          <div style="margin: 10px">
                              <div class="clearfix">
                                  <div class="float-right">
                                      <div style="font-size: 12px; color: black">
                                        ${data.user.user_name}
                                      </div>
                                  </div>
                              </div>
                              <div class="clearfix">
                                  <div class="balloon2 float-right">
                                    ${data.chat_data[i].comment}
                                  </div>
                              </div>
                              <div class="clearfix">
                                  <div class="float-right"
                                      style="text-align: right">
                                      <time style="font-size: 12px">
                                        <a class="read-text">${is_read_string}</a>
                                        ${data.chat_data[i].created_at}
                                      </time>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <br />
                    `;
                    } else {
                        //ファイル
                        if (data.chat_data[i].comment == null) {
                            var html = `
                            <div
                                class="d-flex justify-content-between align-items-center w-100">
                                <strong></strong>
                                <div style="margin: 10px">
                                    <div class="clearfix">
                                        <div class="float-right">
                                            <div style="font-size: 12px; color: black">
                                              ${data.user.user_name}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="balloon2 float-right">
                                          <a href="/api/chatroom/${data.chat_data[i].file_code}/download">${data.chat_data[i].file_name}</a>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="float-right"
                                            style="text-align: right">
                                            <time style="font-size: 12px">
                                              <a class="read-text">${is_read_string}</a>
                                              ${data.chat_data[i].created_at}
                                            </time>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                          `;

                        } else {
                            var html = `
                            <div
                                class="d-flex justify-content-between align-items-center w-100">
                                <strong></strong>
                                <div style="margin: 10px">
                                    <div class="clearfix">
                                        <div class="float-right">
                                            <div style="font-size: 12px; color: black">
                                              ${data.user.user_name}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="balloon2 float-right">
                                          ${data.chat_data[i].comment}
                                        </div>
                                    </div>
                                    <br />
                                    <div class="clearfix">
                                        <div class="balloon2 float-right">
                                          <a href="/api/chatroom/${data.chat_data[i].file_code}/download">${data.chat_data[i].file_name}</a>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="float-right"
                                            style="text-align: right">
                                            <time style="font-size: 12px">
                                              <a class="read-text">${is_read_string}</a>
                                              ${data.chat_data[i].created_at}
                                            </time>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                          `;
                        }
                    }

                } else {
                    //相手側
                    //コメント
                    if (data.chat_data[i].comment_type == 1) {
                        var html = `
                        <br />
                        <div>
                            <div style="font-size: 12px; color: black">
                             ${data.member[i]} 
                            </div>
                            <div class="clearfix">
                                <div class="balloon1 float-left">
                                    <div class="black-text">
                                      ${data.chat_data[i].comment}
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix">
                                <time style="font-size: 12px">
                                  ${data.chat_data[i].created_at}
                                </time>
                            </div>
                        </div>
                        `;

                    } else {

                        //ファイル
                        if (data.chat_data[i].comment == null) {
                            var html = `
                            <br />
                            <div>
                                <div style="font-size: 12px; color: black">
                                 ${data.member[i]} 
                                </div>
                                <div class="clearfix">
                                    <div class="balloon1 float-left">
                                        <div class="black-text">
                                            <a href="/api/chatroom/${data.chat_data[i].file_code}/download">${data.chat_data[i].file_name}</a>
                                        </div>
                                    </div>
                                </div>
                                <div className="clearfix">
                                    <time style="font-size: 12px">
                                      ${data.chat_data[i].created_at}
                                    </time>
                                </div>
                            </div>
                            `;
                        } else {
                            var html = `
                            <br />
                            <div>
                                <div style="font-size: 12px; color: black">
                                 ${data.member[i]}
                                </div>
                                <div class="clearfix">
                                    <div class="balloon1 float-left">
                                        <div class="black-text">
                                          ${data.chat_data[i].comment}
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="clearfix">
                                    <div class="balloon1 float-left">
                                        <div class="black-text">
                                            <a href="/api/chatroom/${data.chat_data[i].file_code}/download">${data.chat_data[i].file_name}</a>
                                        </div>
                                    </div>
                                </div>
                                <div className="clearfix">
                                    <time style="font-size: 12px">
                                      ${data.chat_data[i].created_at}
                                    </time>
                                </div>
                            </div>
                            `;
                        }
                    }
                }
                $("#list").append(html);
            }
            $("#loading").addClass("not-disp");
            $("#message_list").removeClass("not-disp");
        },
    }).done(function () {
        // 成功時の処理
    }).fail(function () {
        // エラー時の処理
        alert("通信エラーです。やり直してください。");
    });
}

//５秒おきにチャットデータAPIを呼び出し
function get_data() {
    var url = set_url("chat_data");
    $.ajax({
        type: "get",
        url: url,
        dataType: "json",
        success: data => {
            $("#list").empty();
            for (var i = 0; i < data.chat_data.length; i++) {
                //既読チェック
                if (chat_type == "chat_member") {
                    if (data.chat_data[i].read_type == 2) {
                        is_read_string = "既読";
                    } else {
                        is_read_string = "";
                    }
                }
                else {
                    is_read_string = "";
                }

                //自分側
                if (data.chat_data[i].sender_user_code == data.user.user_code) {
                    //コメント
                    if (data.chat_data[i].comment_type == 1) {
                        var html = `
                      <div
                          class="d-flex justify-content-between align-items-center w-100">
                          <strong></strong>
                          <div style="margin: 10px">
                              <div class="clearfix">
                                  <div class="float-right">
                                      <div style="font-size: 12px; color: black">
                                        ${data.user.user_name}
                                      </div>
                                  </div>
                              </div>
                              <div class="clearfix">
                                  <div class="balloon2 float-right">
                                    ${data.chat_data[i].comment}
                                  </div>
                              </div>
                              <div class="clearfix">
                                  <div class="float-right"
                                      style="text-align: right">
                                      <time style="font-size: 12px">
                                        <a class="read-text">${is_read_string}</a>
                                        ${data.chat_data[i].created_at}
                                      </time>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <br />
                    `;
                    } else {
                        //ファイル
                        if (data.chat_data[i].comment == null) {
                            var html = `
                            <div
                                class="d-flex justify-content-between align-items-center w-100">
                                <strong></strong>
                                <div style="margin: 10px">
                                    <div class="clearfix">
                                        <div class="float-right">
                                            <div style="font-size: 12px; color: black">
                                              ${data.user.user_name}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="balloon2 float-right">
                                          <a href="/api/chatroom/${data.chat_data[i].file_code}/download">${data.chat_data[i].file_name}</a>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="float-right"
                                            style="text-align: right">
                                            <time style="font-size: 12px">
                                              <a class="read-text">${is_read_string}</a>
                                              ${data.chat_data[i].created_at}
                                            </time>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                          `;

                        } else {
                            var html = `
                            <div
                                class="d-flex justify-content-between align-items-center w-100">
                                <strong></strong>
                                <div style="margin: 10px">
                                    <div class="clearfix">
                                        <div class="float-right">
                                            <div style="font-size: 12px; color: black">
                                              ${data.user.user_name}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="balloon2 float-right">
                                          ${data.chat_data[i].comment}
                                        </div>
                                    </div>
                                    <br />
                                    <div class="clearfix">
                                        <div class="balloon2 float-right">
                                          <a href="/api/chatroom/${data.chat_data[i].file_code}/download">${data.chat_data[i].file_name}</a>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <div class="float-right"
                                            style="text-align: right">
                                            <time style="font-size: 12px">
                                              <a class="read-text">${is_read_string}</a>
                                              ${data.chat_data[i].created_at}
                                            </time>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                          `;
                        }
                    }

                } else {
                    //相手側
                    //コメント
                    if (data.chat_data[i].comment_type == 1) {
                        var html = `
                        <br />
                        <div>
                            <div style="font-size: 12px; color: black">
                            ${data.member[i]} 
                                
                            </div>
                            <div class="clearfix">
                                <div class="balloon1 float-left">
                                    <div class="black-text">
                                      ${data.chat_data[i].comment}
                                    </div>
                                </div>
                            </div>
                            <div className="clearfix">
                                <time style="font-size: 12px">
                                  ${data.chat_data[i].created_at}
                                </time>
                            </div>
                        </div>
                        `;

                    } else {

                        //ファイル
                        if (data.chat_data[i].comment == null) {
                            var html = `
                            <br />
                            <div>
                                <div style="font-size: 12px; color: black">
                                ${data.member[i]} 
                                </div>
                                <div class="clearfix">
                                    <div class="balloon1 float-left">
                                        <div class="black-text">
                                            <a href="/api/chatroom/${data.chat_data[i].file_code}/download">${data.chat_data[i].file_name}</a>
                                        </div>
                                    </div>
                                </div>
                                <div className="clearfix">
                                    <time style="font-size: 12px">
                                      ${data.chat_data[i].created_at}
                                    </time>
                                </div>
                            </div>
                            `;
                        } else {
                            var html = `
                            <br />
                            <div>
                                <div style="font-size: 12px; color: black">
                                ${data.member[i]} 
                                </div>
                                <div class="clearfix">
                                    <div class="balloon1 float-left">
                                        <div class="black-text">
                                          ${data.chat_data[i].comment}
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="clearfix">
                                    <div class="balloon1 float-left">
                                        <div class="black-text">
                                            <a href="/api/chatroom/${data.chat_data[i].file_code}/download">${data.chat_data[i].file_name}</a>
                                        </div>
                                    </div>
                                </div>
                                <div className="clearfix">
                                    <time style="font-size: 12px">
                                      ${data.chat_data[i].created_at}
                                    </time>
                                </div>
                            </div>
                            `;
                        }
                    }
                }
                $("#list").append(html);
            }
        },
    }).done(function () {
        // 成功時の処理
        
    }).fail(function () {
        // エラー時の処理
        alert("通信エラーです。やり直してください。");
    });
    //お好みで変更（現在5秒で関数をよんでいる）
    setTimeout("get_data()", 5000);
}



