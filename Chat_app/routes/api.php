<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
//ログイン後
Route::middleware('auth:user')->group(function () {
    //チャット作成api
    Route::post('chatroom/create', 'ChatAPIController@ChatRoomCreate');
    //グループチャット作成api
    Route::post('chatroom/group/create', 'ChatAPIController@GroupChatRoomCreate');

    //ファイルアップロードapi
    Route::post('chatroom/file/upload', 'ChatAPIController@FileUpload');
    //グループチャットファイルアップロードapi
    Route::post('chatroom/group/file/upload', 'ChatAPIController@GroupFileUpload');

    //チャットデータ取得api
    Route::get('chatroom/{id}/get/{number}', 'ChatAPIController@ChatData');
    //グループチャットデータ取得api
    Route::get('chatroom/{id}/group/get/{number}', 'ChatAPIController@GroupChatData');

    //ファイルダウンロードapi
    Route::get('chatroom/{id}/download', 'ChatAPIController@FileDownload');

});
