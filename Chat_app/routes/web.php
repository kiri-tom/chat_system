<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::view('/', 'welcome');

//ログイン前
Route::group(['prefix' => 'user'], function() {
    Route::get('/',         function () { return redirect('/user/home'); });
    Route::get('login',     'LoginController@showLoginForm')->name('login');
    Route::post('login',    'LoginController@login');
});
//ログイン後
Route::group(['prefix' => 'user', 'middleware' => 'auth:user'], function() {
    Route::post('logout',   'LoginController@logout')->name('user.logout');
    Route::get('home',      'HomeController@index')->name('user.home');

    //チャット
    Route::get('/chatroom', 'ChatRoomController@ChatRoom')->name('chatroom');
    //１対１のチャット作成
    Route::post('/chatroom/create', 'ChatRoomController@ChatRoomCreate');
    //グループチャット作成
    Route::post('/chatroom/group/create', 'ChatRoomController@ChatRoomGroupCreate');

    //チャット受信履歴
    Route::get('/chatroom/reception/history', 'ChatRoomController@ChatReceptionHistory')->name('reception_history');
    //チャット送信履歴
    Route::get('/chatroom/transmission/history', 'ChatRoomController@ChatTransmissionHistory')->name('transmission_history');
});
//ユーザー作成画面
Route::get('/create', 'ChatUserController@create')->name('register');
//ユーザー登録
Route::post('/store', 'ChatUserController@store');

