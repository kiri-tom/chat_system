@extends('layouts.app_user')
<!--チャットルームのcssファイル-->
<link rel="stylesheet" href="{{ asset('css/chatroom.css?v=1.0') }}">

@section('content')
    <main class="container">
        <div class="row">
            <!-- 2列をサイドメニューに割り当て -->
            <div class="col-md-12 blog-sidebar">
                @if(count($chat_collection_reception_history) > 0)
                        <table class="table table-hover">
                            <thead class="">
                            <tr>
                                <th scope="
                                col">
                                </th>
                                <th scope="col">送信者</th>
                                <th scope="col">内容</th>
                                <th scope="col">日付</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($chat_collection_reception_history as $item)
                                    <tr>
                                        <th scope="row"></th>
                                        <td>{{ $item['sender_user_name'] }}</td>
                                        @if ($item['comment_type'] == 1)
                                            <td><p class="text-over">{{ $item['comment'] }}</p></td>
                                        @elseif ($item["comment_type"]== 2)
                                            <td>ファイルを送信しました。</td>
                                        @endif
                                        <td>{{ $item['created_at'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <div class="container">
                            <div class="d-flex align-items-center justify-content-center" style="height:300px;">
                                <h1 class="my-3 h3">受信履歴はありません。</h1>
                            </div>
                        </div>
                        @endif
                        <div class="d-grid gap-2 col-6 mx-auto">
                            {{ $chat_collection_reception_history->links() }}
                        </div>
                        <div class="d-grid gap-2 col-6 mx-auto">
                            <a href="/user/chatroom"><button type="button" class="btn btn-secondary" id="">チャットへ戻る</button></a>
                        </div>
            </div>
        </div>
    </main>
    <script>
    </script>

@endsection
