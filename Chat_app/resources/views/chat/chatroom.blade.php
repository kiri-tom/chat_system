@extends('layouts.app_user')
<!--チャットルームのcssファイル-->
<link rel="stylesheet" href="{{ asset('css/chatroom.css?v=1.0') }}">

@section('content')
    <main class="container">
        <div class="row">
            <!-- 2列をサイドメニューに割り当て -->
            <div class="col-md-12 blog-sidebar">
                <div class="tab-wrap">
                    <input id="TAB-01" type="radio" name="TAB" class="tab-switch" checked="checked" /><label
                        class="tab-label" for="TAB-01">メッセージ一覧</label>
                    <div class="tab-content">
                        <main class="container">
                            <div class="row">
                                <!-- 4列をサイドメニューに割り当て -->
                                <div class="col-md-4 blog-sidebar">
                                    <div class="my-3 p-3 bg-white rounded shadow-sm">
                                        <div class="form-check">
                                            <input id="type_group" type="radio" name="type" class="form-check-input" />
                                            <label class="d-block" for="type_group" class="form-check-label">
                                                <h6 class="border-bottom border-gray pb-2 mb-0">グループ</h6>
                                            </label>
                                        </div>
                                        <div id="chat_group" class="not-disp">
                                            @foreach ($chat_list_group as $key => $item)
                                                <div class="media text-muted pt-3 hover-member" id="hover">
                                                    <img data-src="holder.js/32x32?theme=thumb&amp;bg=007bff&amp;fg=007bff&amp;size=1"
                                                        alt="" class="mr-2 rounded">
                                                    <div
                                                        class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                                        <div
                                                            class="d-flex justify-content-between align-items-center w-100">
                                                            <strong class="text-gray-dark">グループ名</strong>
                                                        </div>
                                                        <!-- グループ名 -->
                                                        <div class="form-check">
                                                            <input id="group_{{ $key }}" type="radio" name="member"
                                                                data-code="{{ $item->chat_list_code }}"
                                                                class="form-check-input" />
                                                            <label class="d-block" for="group_{{ $key }}"
                                                                class="form-check-label">{{ $item->group_name }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="my-3 p-3 bg-white rounded shadow-sm">
                                        <div class="form-check">
                                            <input id="type_member" type="radio" name="type" class="form-check-input" />
                                            <label class="d-block" for="type_member" class="form-check-label">
                                                <h6 class="border-bottom border-gray pb-2 mb-0">メンバー</h6>
                                            </label>
                                        </div>
                                        <div id="chat_member" class="not-disp">
                                            @foreach ($chat_lists as $key => $item)
                                                <div class="media text-muted pt-3 hover-member" id="hover">
                                                    <img data-src="holder.js/32x32?theme=thumb&amp;bg=007bff&amp;fg=007bff&amp;size=1"
                                                        alt="" class="mr-2 rounded">
                                                    <div
                                                        class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                                        <div
                                                            class="d-flex justify-content-between align-items-center w-100">
                                                            <strong class="text-gray-dark">名前</strong>
                                                        </div>
                                                        <!-- ユーザー名 -->
                                                        <div class="form-check">
                                                            <input id="user_{{ $key }}" type="radio" name="member"
                                                                data-code="{{ $item->chat_list_code }}"
                                                                class="form-check-input" />
                                                            <label class="d-block" for="user_{{ $key }}"
                                                                class="form-check-label">{{ $item->user->user_name }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <!-- 残り8列はコンテンツ表示部分として使う -->
                                <div class="col-md-8 blog-main">
                                    <div class="row">
                                        <article>
                                            <section>
                                                <!--foreach ($chat_lists as $key => $item)-->
                                                @if (count($chat_lists) > 0)
                                                    <div class="message-bc not-disp" id="list_0">
                                                        <div id="loading"
                                                            class="">
                                                            <div class="
                                                            d-flex justify-content-center">
                                                            <div class="spinner-border" role="status">
                                                                <span class="sr-only"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="message_list">
                                                        <div id="more_list_div">
                                                            <div class="d-grid gap-2 col-6 mx-auto">
                                                                <button type="button" class="btn btn-secondary"
                                                                    id="more_list">さらに</button>
                                                            </div>
                                                        </div>
                                                        <div id="list">
                                                            <!-- @foreach ($chat_histry as $list)
                                                                    @if ($list->sender_user_code == $user->user_code)
                                                                         右吹き出し 
                                                                        <div
                                                                            class="d-flex justify-content-between align-items-center w-100">
                                                                            <strong></strong>
                                                                            <div style="margin: 10px">
                                                                                <div class="clearfix">
                                                                                    <div class="float-right">
                                                                                        <div style="font-size: 12px; color: black">
                                                                                            {{ $list->user_name }}
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="clearfix">
                                                                                    <div class="balloon2 float-right">
                                                                                        {{ $list->comment }}
                                                                                    </div>
                                                                                </div>
                                                                                @if ($list->comment_type == 2)
                                                                                    <br />
                                                                                    <div class="clearfix">
                                                                                        <div class="balloon2 float-right">
                                                                                            <a
                                                                                                href="/api/chatroom/${data.chat_data[i].file_code}/download">{{ $list->file_name }}</a>
                                                                                        </div>
                                                                                    </div>
                                                                                @endif
                                                                                <div class="clearfix">
                                                                                    <div class="float-right"
                                                                                        style="text-align: right">
                                                                                        <time style="font-size: 12px">
                                                                                            @if ($list->read_type == 2)
                                                                                                <a class="read-text">既読</a>
                                                                                            @endif
                                                                                            {{ $list->created_at }}
                                                                                        </time>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                            @else
                                                                         改行 
                                                                        <br />
                                                                         左吹き出し 
                                                                        <div>
                                                                            <div style="font-size: 12px; color: black">
                                                                                {{ $list->user_name }}
                                                                            </div>
                                                                            <div class="clearfix">
                                                                                <div class="balloon1 float-left">
                                                                                    <div class="black-text">
                                                                                        {{ $list->comment }}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            @if ($list->comment_type == 2)
                                                                                <br />
                                                                                <div class="clearfix">
                                                                                    <div class="balloon2 float-right">
                                                                                        <a
                                                                                            href="/api/chatroom/${data.chat_data[i].file_code}/download">{{ $list->file_name }}</a>
                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                            <div className="clearfix">
                                                                                <time style="font-size: 12px">
                                                                                    {{ $list->created_at }}
                                                                                </time>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endforeach -->
                                                        </div>
                                                    </div>
                                    </div>
                                    <div class="not-disp" id="input_field_0">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Comment..."
                                                aria-describedby="button-addon2" id="api_message">
                                            <button class="btn btn-outline-secondary" type="button"
                                                id="send_message">送信</button>
                                        </div>
                                        <div class="input-group">
                                            <input type="file" class="form-control" id="api_file"
                                                aria-describedby="inputGroupFileAddon04" aria-label="Upload">
                                            <button class="btn btn-outline-secondary" type="button"
                                                id="send_upload">アップロード</button>
                                        </div>
                                    </div>
                                    </form>
                                    @endif
                                    <!--endforeach-->
                                    </section>
                                    </article>
                                </div>
                            </div>
                    </div>
    </main>
    </div>
    <input id="TAB-02" type="radio" name="TAB" class="tab-switch" /><label class="tab-label"
        for="TAB-02">新規作成</label>
    <div class="tab-content">
        <div class="side-tab-wrap">
            <input id="tab01" type="radio" name="tab" class="side-tab-switch" checked="checked"><label
                class="side-tab-label" for="tab01">個人</label>
            <div class="side-tab-content">
                <form id="individual_form" action="/user/chatroom/create" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">宛先</label>
                        @foreach ($users as $key => $val)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="user_code_radio"
                                    id="user_check_{{ $key }}" value="{{ $val->user_code }}">
                                <label class="form-check-label"
                                    for="user_check_{{ $key }}">{{ $val->user_name }}</label>
                            </div>
                        @endforeach
                        <div id="emailHelp" class="form-text">※一人選択してください。
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">メッセージ</label>
                        <textarea class="form-control row-textarea" aria-label="With textarea" name="comment"
                            id="form_message"></textarea>
                    </div>
                    <div class="mb-3">
                        <label for="formFile" class="form-label">ファイル選択</label>
                        <input class="form-control" type="file" id="formFile" name="file">
                    </div>
                    <button type="submit" class="btn btn-primary">送信</button>
                </form>
            </div>
            <input id="tab02" type="radio" name="tab" class="side-tab-switch"><label class="side-tab-label"
                for="tab02">グループ</label>
            <div class="side-tab-content">
                <form id="group_form" action="/user/chatroom/group/create" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <div class="form-group">
                            <label class="control-label">グループ名</label>
                            <input class="form-control" type="text" name="group_name" id="group_name_id">
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">追加メンバー</label>
                        @foreach ($users as $key => $val)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" name="user_code_check[]"
                                    id="user_checkbox_{{ $key }}" value="{{ $val->user_code }}"
                                    data-check="check_box">
                                <label class="form-check-label"
                                    for="user_checkbox_{{ $key }}">{{ $val->user_name }}</label>
                            </div>
                        @endforeach
                        <div id="emailHelp" class="form-text">※一人以上選択してください。
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">メッセージ</label>
                        <textarea class="form-control row-textarea" aria-label="With textarea" name="comment"
                            id="form_group_message"></textarea>
                    </div>
                    <div class="mb-3">
                        <label for="form_group_File" class="form-label">ファイル選択</label>
                        <input class="form-control" type="file" id="form_group_File" name="file">
                    </div>
                    <button type="submit" class="btn btn-primary">送信</button>
                </form>
            </div>
        </div>
    </div>
    <input id="TAB-03" type="radio" name="TAB" class="tab-switch" /><label class="tab-label"
        for="TAB-03">受信履歴</label>
    <div class="tab-content">
        @if (count($chat_collection_reception_history) > 0)
            <table class="table table-hover">
                <thead class="">
                            <tr>
                                <th scope=" col">
                    </th>
                    <th scope="col">送信者</th>
                    <th scope="col">内容</th>
                    <th scope="col">日付</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($chat_collection_reception_history as $item)
                        <tr>
                            <th scope="row"></th>
                            <td>{{ $item['sender_user_name'] }}</td>
                            @if ($item['comment_type'] == 1)
                                <td>
                                    <p class="text-over">{{ $item['comment'] }}</p>
                                </td>
                            @elseif ($item["comment_type"]== 2)
                                <td>ファイルを送信しました。</td>
                            @endif
                            <td>{{ $item['created_at'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @if (count($chat_collection_reception_history) > 50)
                <div id="">
                    <div class="d-grid gap-2 col-6 mx-auto">
                        <a href="/user/chatroom/reception/history"><button type="button" class="btn btn-secondary"
                                id="">さらに履歴を見る</button></a>
                    </div>
                </div>
            @endif
        @else
            <div class="container">
                <div class="d-flex align-items-center justify-content-center" style="height:300px;">
                    <h1 class="my-3 h3">受信履歴はありません。</h1>
                </div>
            </div>
        @endif
    </div>
    <input id="TAB-04" type="radio" name="TAB" class="tab-switch" /><label class="tab-label"
        for="TAB-04">送信履歴</label>
    <div class="tab-content">
        @if (count($chat_transmission_history) > 0)
            <table class="table table-hover">
                <thead class="">
                            <tr>
                                <th scope=" col">
                    </th>
                    <th scope="col">送信先</th>
                    <th scope="col">内容</th>
                    <th scope="col">日付</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($chat_transmission_history as $item)
                        <tr>
                            <th scope="row"></th>
                            <td>{{ $item['recipient_user_name'] }}</td>
                            @if ($item['comment_type'] == 1)
                                <td>
                                    <p class="text-over">{{ $item['comment'] }}</p>
                                </td>
                            @elseif ($item["comment_type"]== 2)
                                <td>ファイルを送信しました。</td>
                            @endif
                            <td>{{ $item['created_at'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @if (count($chat_transmission_history) > 50)
                <div id="">
                    <div class="d-grid gap-2 col-6 mx-auto">
                        <a href="/user/chatroom/transmission/history"><button type="button" class="btn btn-secondary"
                                id="">さらに履歴を見る</button></a>
                    </div>
                </div>
            @endif
        @else
            <div class="container">
                <div class="d-flex align-items-center justify-content-center" style="height:300px;">
                    <h1 class="my-3 h3">送信履歴はありません。</h1>
                </div>
            </div>
        @endif
    </div>
    </div>
    </div>
    </div>
    </main>
    <script src="{{ asset('js/chat.js') }}"></script>
    <script>
        //チェックボタンデータ
        var checked_button;
        //チャットID
        var list_code;
        //グループかメンバーか
        var chat_type;
        //既読
        var is_read_string = "";
        //チャット履歴取得数初期値
        var list_number_first = 100;
        //チャット履歴取得数
        var list_number = 100;
        //チャット履歴追加取得数
        var list_number_plus = 100;
        //初期表示
        $(window).on('load', function() {
            get_data();
        });
    </script>

@endsection
