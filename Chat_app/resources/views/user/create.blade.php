@extends('layouts.app_user')

@section('content')
<div class="container">
<div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header">ユーザー登録</div>
            <div class="card-body">
          <form method="POST" action="/store">
              @csrf
            <div class="form-group">
              <label class="control-label">ユーザーネーム</label>
              <input class="form-control" type="text" name="user_name">
            </div>
            <div class="form-group">
              <label class="control-label">メールアドレス</label>
              <input class="form-control" type="text" name="email">
            </div>
            <div class="form-group">
              <label class="control-label">パスワード</label>
              <input class="form-control" type="text" name="password">
            </div>
            <div class="form-group">
              <label class="control-label">組織名</label>
              <input class="form-control" type="text" name="oganization_name">
            </div>
            
            <a><button class='btn btn-primary'>登録</button></a>
          </form>
          </div>
          </div>
        </div>
      </div>
    </div>
@endsection
