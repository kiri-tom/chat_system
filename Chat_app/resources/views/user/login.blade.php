@extends('layouts.app_user')

@section('content')
<div class="container">
    <div class="panel panel-default">
    <div class="panel-heading">フォーム1</div>
        <div class="panel-body">
          <form method="POST" action="{{ route('login') }}">
              @csrf
            <div class="form-group">
              <label class="control-label">メールアドレス</label>
              <input class="form-control" type="text" name="email">
            </div>
            <div class="form-group">
              <label class="control-label">パスワード</label>
              <input class="form-control" type="text" name="password">
            </div>
            <button class="btn btn-default">送信</button>
          </form>
        </div>
      </div>
    </div>
@endsection
