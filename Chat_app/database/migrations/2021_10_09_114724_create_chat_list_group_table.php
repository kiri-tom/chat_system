<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatListGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_list_group', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('chat_list_code',10)->nullable();
            $table->string('user_code',10)->nullable();
            $table->string('group_name',50)->nullable();
            $table->integer('read_type')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_list_group');
    }
}
