<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('chat_list_code',10)->nullable();
            $table->string('sender_user_code',10)->nullable();
            $table->string('receiver_user_code',10)->nullable();
            $table->text('comment')->nullable();
            $table->string('file_code')->nullable();
            $table->string('file_name')->nullable();
            $table->integer('comment_type')->nullable();
            $table->integer('read_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat');
    }
}
