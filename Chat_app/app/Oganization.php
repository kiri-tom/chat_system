<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oganization extends Model
{
    protected $table = 'oganization';
    protected $fillable = ['oganization_name','oganization_code'];
}
