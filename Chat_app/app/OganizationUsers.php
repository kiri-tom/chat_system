<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OganizationUsers extends Model
{
    protected $table = 'oganization_users';
    protected $fillable = ['parent_id','child_id'];
}
