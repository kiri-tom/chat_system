<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatGroup extends Model
{
    protected $table = 'chat_group';
    protected $fillable = [
        'chat_list_code',
        'sender_user_code',
        'comment',
        'file_code',
        'file_name',
        'comment_type',
        'read_type',
    ];

    //ユーザーデータ取得
    public function User_data($user_code)
    {
        $user_data = ChatUser::where("user_code",$user_code)->first();
        return $user_data;
    }
    //チャットグループリストデータ取得
    public function List_data($chat_list_code)
    {
        $list_data = ChatListGroup::where("chat_list_code",$chat_list_code)->first();
        return $list_data;
    }
}
