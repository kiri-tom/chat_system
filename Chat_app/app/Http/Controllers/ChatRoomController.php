<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ChatUser;
use App\Chat;
use App\ChatList;
use App\ChatGroup;
use App\ChatListGroup;
use App\ChatFile;
use Illuminate\Support\Facades\Storage;

class ChatRoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user');
    }
    //チャットルーム
    public function ChatRoom()
    {
        $user = Auth::user();
        //チャット履歴用に自身に関係あるチャットリスト取得
        $chat_lists_user = ChatList::where("user_code", $user->user_code)->get();
        $chat_lists = [];
        foreach ($chat_lists_user as $list) {
            $chat_lists[] = ChatList::where("chat_list_code", $list->chat_list_code)->where("user_code", "!=", $user->user_code)->first();
        }
        //チャット履歴用に自身に関係あるグループチャットリスト取得
        $chat_list_group = ChatListGroup::where("user_code", $user->user_code)->get();
        //チャット履歴取得
        $chat_histry = null;
        if (count($chat_lists) > 0) {
            $chat_histry = Chat::where("chat_list_code", $chat_lists[0]->chat_list_code)->get();
        }
        //グループチャット履歴取得
        $chat_group_histry = null;
        if (count($chat_list_group) > 0) {
            $chat_group_histry = ChatGroup::where("chat_list_code", $chat_list_group[0]->chat_list_code)->get();
        }
        //新規作成用に全ユーザー取得
        $users = ChatUser::where("user_code", "!=", $user->user_code)->get();
        //受信履歴取得
        $chat_collection_reception_history = collect([]);
        $chat_reception_history_data = Chat::where("receiver_user_code", $user->user_code)->latest()->take(100)->get();
        foreach ($chat_reception_history_data as $list) {
            $chat_collection_reception_history = $chat_collection_reception_history->concat([
                [
                    //chat_type = 1 が１対１。２がグループ。
                    'chat_type' => 1,
                    'sender_user_name' => $list->User_data($list->sender_user_code)->user_name,
                    'comment' => $list->comment,
                    'comment_type' => $list->comment_type,
                    'created_at' => $list->created_at,
                ],
            ]);
        }
        $Group_chat_reception_history_data = ChatGroup::where("sender_user_code", "!=", $user->user_code)->latest()->take(100)->get();
        foreach ($Group_chat_reception_history_data as $list) {
            $chat_collection_reception_history = $chat_collection_reception_history->concat([
                [
                    //chat_type = 1 が１対１。２がグループ。
                    'chat_type' => 2,
                    'sender_user_name' => $list->List_data($list->chat_list_code)->group_name."（".$list->User_data($list->sender_user_code)->user_name."）",
                    'comment' => $list->comment,
                    'comment_type' => $list->comment_type,
                    'created_at' => $list->created_at,
                ],
            ]);
        }
        //受信履歴を作成時間でソート
        $chat_collection_reception_history = $chat_collection_reception_history->sortByDesc('created_at')->take(50);
        //送信履歴取得
        $chat_transmission_history = collect([]);
        $chat_transmission_history_data = Chat::where("sender_user_code", $user->user_code)->latest()->take(100)->get();
        foreach ($chat_transmission_history_data as $list) {
            $chat_transmission_history = $chat_transmission_history->concat([
                [
                    //chat_type = 1 が個別。２がグループ。
                    'chat_type' => 1,
                    'recipient_user_name' => $list->User_data($list->receiver_user_code)->user_name,
                    'comment' => $list->comment,
                    'comment_type' => $list->comment_type,
                    'created_at' => $list->created_at,
                ],
            ]);
        }
        $Group_chat_transmission_history_data = ChatGroup::where("sender_user_code",  $user->user_code)->latest()->take(100)->get();
        foreach ($Group_chat_transmission_history_data as $list) {
            $chat_transmission_history = $chat_transmission_history->concat([
                [
                    //chat_type = 1 が個別。２がグループ。
                    'chat_type' => 2,
                    'recipient_user_name' => $list->List_data($list->chat_list_code)->group_name,
                    'comment' => $list->comment,
                    'comment_type' => $list->comment_type,
                    'created_at' => $list->created_at,
                ],
            ]);
        }
        //送信履歴を作成時間でソート
        $chat_transmission_history = $chat_transmission_history->sortByDesc('created_at')->take(50);
        //\Debugbar::info($sorted);
        //return view("welcome");
        $data = [
            "user" => $user,
            "users" => $users,
            "chat_lists" => $chat_lists,
            "chat_list_group" => $chat_list_group,
            "chat_histry" => $chat_histry,
            "chat_group_histry" => $chat_group_histry,
            "chat_collection_reception_history" => $chat_collection_reception_history,
            "chat_transmission_history" => $chat_transmission_history,
        ];
        return view("chat.chatroom", $data);
    }
    //１対１のチャットルーム作成
    public function ChatRoomCreate(Request $request)
    {
        $user = Auth::user();
        $request_data = $request->all();
        $file = $request->file("file");
        $path = null;
        $file_name = null;
        \Debugbar::info($file);
        //コメントタイプ：１＝コメントのみ。２＝ファイル送信
        $comment_type = 1;
        if ($request->file("file") == null) {
            $comment_type = 1;
        } else {
            $comment_type = 2;
        }
        //すでに１対１の相手がいる場合
        if (
            ChatRoomController::ExistsChatListCheck($user->user_code,$request_data["user_code_radio"])
        ) {
            $chat_list = ChatList::where("user_code", $request_data["user_code_radio"])->first();
            $chat_code = $chat_list->chat_list_code;
            $file_code = ChatRoomController::ID(20);
            //ファイル登録
            if ($request->hasFile('file')) {
                $file_name = $file->getClientOriginalName();
                $path = $file->storeAs('chat_file/' . $chat_code . '/' . $user->user_code, $file_name);
            }
            //チャットデータ作成
            Chat::create([
                "chat_list_code" => $chat_code,
                "sender_user_code" => $user->user_code,
                "receiver_user_code" => $request_data["user_code_radio"],
                "comment" => $request_data["comment"],
                "file_code" => $file_code,
                "file_name" => $file_name,
                "comment_type" => $comment_type,
                "read_type" => 1,
            ]);
            //ファイルデータ作成
            ChatFile::create([
                "chat_list_code" => $chat_code,
                "file_code" => $file_code,
                "file_path" => $path,
                "file_name" => $file_name,
            ]);
        } else {
            //新規の人
            //チャットコード作成
            $chat_code = ChatRoomController::ID(10);
            $file_code = ChatRoomController::ID(20);
            //ファイル登録
            if ($request->hasFile('file')) {
                $file_name = $file->getClientOriginalName();
                $path = $file->storeAs('chat_file/' . $chat_code . '/' . $user->user_code, $file_name);
            }
            //ファイルデータ作成
            ChatFile::create([
                "chat_list_code" => $chat_code,
                "file_code" => $file_code,
                "file_path" => $path,
                "file_name" => $file_name,
            ]);
            //チャットデータ作成
            Chat::create([
                "chat_list_code" => $chat_code,
                "sender_user_code" => $user->user_code,
                "receiver_user_code" => $request_data["user_code_radio"],
                "comment" => $request_data["comment"],
                "file_code" => $file_code,
                "file_name" => $file_name,
                "comment_type" => $comment_type,
                "read_type" => 1,
            ]);
            //チャットリスト管理（自分）
            ChatList::create([
                "chat_list_code" => $chat_code,
                "user_code" => $user->user_code,
            ]);
            //チャットリスト管理（相手）
            ChatList::create([
                "chat_list_code" => $chat_code,
                "user_code" => $request_data["user_code_radio"],
            ]);
        }
        //return view("welcome");
        return redirect(route("chatroom"));
    }

    //グループチャットルーム作成
    public function ChatRoomGroupCreate(Request $request)
    {
        $user = Auth::user();
        $request_data = $request->all();
        $file = $request->file("file");
        $path = null;
        $file_name = null;
        //コメントタイプ：１＝コメントのみ。２＝ファイル送信
        if ($request->file("file") == null) {
            $comment_type = 1;
        } else {
            $comment_type = 2;
        }
        // \Debugbar::info($comment_type);
        // return view("welcome");
        //チャットコード作成
        $chat_code = ChatRoomController::ID(10);
        $file_code = ChatRoomController::ID(20);
        //ファイル登録
        if ($request->hasFile('file')) {
            $file_name = $file->getClientOriginalName();
            $path = $file->storeAs('chat_file/' . $chat_code . '/' . $user->user_code, $file_name);
            //ファイルデータ作成
            ChatFile::create([
                "chat_list_code" => $chat_code,
                "file_code" => $file_code,
                "file_path" => $path,
                "file_name" => $file_name,
            ]);
        }
        //チャットデータ作成
        ChatGroup::create([
            "chat_list_code" => $chat_code,
            "sender_user_code" => $user->user_code,
            "comment" => $request_data["comment"],
            "file_code" => $file_code,
            "file_name" => $file_name,
            "comment_type" => $comment_type,
        ]);
        //チャットリスト管理（自分）
        ChatListGroup::create([
            "chat_list_code" => $chat_code,
            "user_code" => $user->user_code,
            "group_name" => $request_data["group_name"],
        ]);
        foreach ($request_data["user_code_check"] as $user_code) {
            //チャットリスト管理（相手）
            ChatListGroup::create([
                "chat_list_code" => $chat_code,
                "user_code" => $user_code,
                "group_name" => $request_data["group_name"],
            ]);
        }
        //return view("welcome");
        return redirect(route("chatroom"));
    }

    //受信履歴画面
    public function ChatReceptionHistory()
    {
        $user = Auth::user();
        //受信履歴取得
        $chat_collection_reception_history = collect([]);
        $chat_reception_history_data = Chat::where("receiver_user_code", $user->user_code)->latest()->get();
        foreach ($chat_reception_history_data as $list) {
            $chat_collection_reception_history = $chat_collection_reception_history->concat([
                [
                    //chat_type = 1 が１対１。２がグループ。
                    'chat_type' => 1,
                    'sender_user_name' => $list->User_data($list->sender_user_code)->user_name,
                    'comment' => $list->comment,
                    'comment_type' => $list->comment_type,
                    'created_at' => $list->created_at,
                ],
            ]);
        }
        $Group_chat_reception_history_data = ChatGroup::where("sender_user_code", "!=", $user->user_code)->latest()->get();
        foreach ($Group_chat_reception_history_data as $list) {
            $chat_collection_reception_history = $chat_collection_reception_history->concat([
                [
                    //chat_type = 1 が１対１。２がグループ。
                    'chat_type' => 2,
                    'sender_user_name' => $list->List_data($list->chat_list_code)->group_name."（".$list->User_data($list->sender_user_code)->user_name."）",
                    'comment' => $list->comment,
                    'comment_type' => $list->comment_type,
                    'created_at' => $list->created_at,
                ],
            ]);
        }
        //受信履歴を作成時間でソート
        $chat_collection_reception_history = $chat_collection_reception_history->sortByDesc('created_at')->paginate(5);
        $data = [
            "user" => $user,
            "chat_collection_reception_history" => $chat_collection_reception_history,
        ];
        return view("chat.chat_reception_history", $data);
    }
    //送信履歴画面
    public function ChatTransmissionHistory()
    {
        $user = Auth::user();
        //送信履歴取得
        $chat_transmission_history = collect([]);
        $chat_transmission_history_data = Chat::where("sender_user_code", $user->user_code)->latest()->get();
        foreach ($chat_transmission_history_data as $list) {
            $chat_transmission_history = $chat_transmission_history->concat([
                [
                    //chat_type = 1 が個別。２がグループ。
                    'chat_type' => 1,
                    'recipient_user_name' => $list->User_data($list->receiver_user_code)->user_name,
                    'comment' => $list->comment,
                    'comment_type' => $list->comment_type,
                    'created_at' => $list->created_at,
                ],
            ]);
        }
        $Group_chat_transmission_history_data = ChatGroup::where("sender_user_code",  $user->user_code)->latest()->get();
        foreach ($Group_chat_transmission_history_data as $list) {
            $chat_transmission_history = $chat_transmission_history->concat([
                [
                    //chat_type = 1 が個別。２がグループ。
                    'chat_type' => 2,
                    'recipient_user_name' => $list->List_data($list->chat_list_code)->group_name,
                    'comment' => $list->comment,
                    'comment_type' => $list->comment_type,
                    'created_at' => $list->created_at,
                ],
            ]);
        }
        //送信履歴を作成時間でソート
        $chat_transmission_history = $chat_transmission_history->sortByDesc('created_at')->paginate(5);
        $data = [
            "user" => $user,
            "chat_transmission_history" => $chat_transmission_history,
        ];
        return view("chat.chat_transmission_history", $data);
    }

    //ID作成
    private function ID($length)
    {
        return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, $length);
    }
    //１対１のチャットがすでに存在しているかチェック
    private function ExistsChatListCheck($user_code,$user_code2)
    {
        $chat_lists_user = ChatList::where("user_code", $user_code)->get();
        $chat_lists_code =[];
        foreach($chat_lists_user as $list){
            $chat_lists_code[] = $list->chat_list_code;
        }
        $chat_lists = ChatList::where("user_code","!=", $user_code)->whereIn("chat_list_code", $chat_lists_code)->get();
        $cnt = 0;
        foreach($chat_lists as $list){
            if($list->user_code == $user_code2){
                $cnt += 1;
            }
        }
        if($cnt > 0){
            return true;
        }else{
            return false;
        }
    }
}
