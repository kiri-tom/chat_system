<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
 
    use AuthenticatesUsers;
 
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/user/home'; // 変更
 
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:user')->except('logout'); //変更
    }
    
    public function showLoginForm()
    {
        return view('user.login');  //変更
    }
 
    protected function guard()
    {
        return Auth::guard('user');  //変更
    }
    
    public function logout(Request $request)
    {
        Auth::guard('user')->logout();  //変更
        $request->session()->flush();
        $request->session()->regenerate();
 
        return redirect('/user/login');  //変更
    }
}
