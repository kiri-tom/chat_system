<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ChatUser;
use App\Chat;
use App\ChatList;
use App\ChatGroup;
use App\ChatListGroup;
use App\ChatFile;
use Illuminate\Support\Facades\Storage;


class ChatAPIController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:user');
    }
    //チャットデータ取得
    public function ChatData(Request $request)
    {
        $user = Auth::user();
        //重くならないようチャットを件数分取得
        $chat_data = Chat::where('chat_list_code', $request->id)->take($request->number)->latest()->get();
        $chat_data_count = count(Chat::where('chat_list_code', $request->id)->get());
        $number_flag = false;
        if($chat_data_count <= $request->number){
            $number_flag = true;
        }
        $collection = collect($chat_data);
        $chat_data = $collection->sortBy('id');
        //最新の件数をソート
        $chat_data_sorted = [];
        foreach ($chat_data as $data) {
            $chat_data_sorted[] = $data;
        }
        $chat_list = ChatList::where("chat_list_code", $request->id)->where("user_code", "!=", $user->user_code)->first();
        //名前を配列に
        $member = [];
        foreach ($chat_data as $data) {
            $member[] = $data->User_data($data->sender_user_code)->user_name;
        }
        //自身の未読のチャットを取得
        $unread_chat_data = Chat::where('chat_list_code', $request->id)->where('sender_user_code', "!=", $user->user_code)->where('read_type', 1)->get();
        //開いたので既読へ
        foreach ($unread_chat_data as $data) {
            $data->read_type = 2;
            $data->save();
        }
        $json = [
            "chat_data" => $chat_data_sorted,
            "user" => $user,
            "member" => $member,
            "chat_list" => $chat_list,
            "number_flag" => $number_flag,
        ];
        return response()->json($json);
    }
    //グループチャットデータ取得
    public function GroupChatData(Request $request)
    {
        $user = Auth::user();
        //重くならないようチャットを件数分取得
        $chat_data = ChatGroup::where('chat_list_code', $request->id)->take($request->number)->latest()->get();
        $chat_list = ChatListGroup::where("chat_list_code", $request->id)->where("user_code", "!=", $user->user_code)->first();
        $chat_data_count = count(ChatGroup::where('chat_list_code', $request->id)->get());
        $number_flag = false;
        if($chat_data_count <= $request->number){
            $number_flag = true;
        }
        $collection = collect($chat_data);
        $chat_data = $collection->sortBy('id');
        //最新の件数をソート
        $chat_data_sorted = [];
        foreach ($chat_data as $data) {
            $chat_data_sorted[] = $data;
        }
        //メンバーを配列に
        $member = [];
        foreach ($chat_data as $data) {
            $member[] = $data->User_data($data->sender_user_code)->user_name;
        }
        //自身の未読のチャットを取得
        //$unread_chat_data = Chat::where('chat_list_code', $request->id)->where('sender_user_code', $partner->user_code)->where('read_type', 1)->get();
        //開いたので既読へ
        // foreach($unread_chat_data as $data){
        //     $data->read_type = 2;
        //     $data->save();
        // }
        $json = [
            "chat_data" => $chat_data_sorted,
            "user" => $user,
            "member" => $member,
            "chat_list" => $chat_list,
            "number_flag" => $number_flag,
        ];
        return response()->json($json);
    }

    //１対１のチャット作成（メッセージ送信）
    public function ChatRoomCreate(Request $request)
    {
        $user = Auth::user();
        $request_data = $request->all();
        //\Debugbar::info($request->file("file"));
        //コメントタイプ：１＝コメントのみ。２＝ファイル送信
        $comment_type = 1;
        //すでに１対１の相手がいる場合
        $chat_code = $request->chat_list_code;
        $chat_list = ChatList::where("chat_list_code", $chat_code)->where("user_code", "!=", $user->user_code)->first();
        //チャットデータ作成
        Chat::create([
            "chat_list_code" => $chat_code,
            "sender_user_code" => $user->user_code,
            "receiver_user_code" => $chat_list->user_code,
            "comment" => $request_data["comment"],
            "comment_type" => $comment_type,
            "read_type" => 1,
        ]);
        $json = "成功";
        return response()->json($json);
    }
    //グループチャット作成（メッセージ送信）
    public function GroupChatRoomCreate(Request $request)
    {
        $user = Auth::user();
        $request_data = $request->all();
        //\Debugbar::info($request->file("file"));
        //コメントタイプ：１＝コメントのみ。２＝ファイル送信
        $comment_type = 1;
        //すでに１対１の相手がいる場合
        $chat_code = $request->chat_list_code;
        $chat_list = ChatListGroup::where("chat_list_code", $chat_code)->where("user_code", "!=", $user->user_code)->first();
        //チャットデータ作成
        ChatGroup::create([
            "chat_list_code" => $chat_code,
            "sender_user_code" => $user->user_code,
            "comment" => $request_data["comment"],
            "comment_type" => $comment_type,
        ]);
        $json = "成功";
        return response()->json($json);
    }
    //ファイルアップロードAPI
    public function FileUpload(Request $request)
    {
        \Debugbar::info($request->file);
        \Debugbar::info($request->chat_list_code);
        $file = $request->file("file");
        $path = null;
        $user = Auth::user();
        //\Debugbar::info($request->file("file"));
        //コメントタイプ：１＝コメントのみ。２＝ファイル送信
        $comment_type = 2;
        //すでに１対１の相手がいる場合
        $chat_code = $request->chat_list_code;
        $chat_list = ChatList::where("chat_list_code", $chat_code)->where("user_code", "!=", $user->user_code)->first();
        //ファイル登録
        if ($request->hasFile('file')) {
            $file_name = $file->getClientOriginalName();
            $path = $file->storeAs('chat_file/' . $chat_code . '/' . $user->user_code, $file_name);
        }
        $file_code = ChatAPIController::ID(20);
        //チャットデータ作成
        Chat::create([
            "chat_list_code" => $chat_code,
            "sender_user_code" => $user->user_code,
            "receiver_user_code" => $chat_list->user_code,
            "comment" => null,
            "file_code" => $file_code,
            "file_name" => $file_name,
            "comment_type" => $comment_type,
            "read_type" => 1,
        ]);
        //ファイルデータ作成
        ChatFile::create([
            "chat_list_code" => $chat_code,
            "file_code" => $file_code,
            "file_path" => $path,
            "file_name" => $file_name,
        ]);
        $json = "成功";
        return response()->json($json);
    }
    //グループファイルアップロードAPI
    public function GroupFileUpload(Request $request)
    {
        \Debugbar::info($request->file);
        \Debugbar::info($request->chat_list_code);
        $file = $request->file("file");
        $path = null;
        $user = Auth::user();
        //\Debugbar::info($request->file("file"));
        //コメントタイプ：１＝コメントのみ。２＝ファイル送信
        $comment_type = 2;
        //すでにグループがある場合
        $chat_code = $request->chat_list_code;
        //ファイル登録
        if ($request->hasFile('file')) {
            $file_name = $file->getClientOriginalName();
            $path = $file->storeAs('chat_file_group/' . $chat_code . '/' . $user->user_code, $file_name);
        }
        $file_code = ChatAPIController::ID(20);
        //チャットデータ作成
        ChatGroup::create([
            "chat_list_code" => $chat_code,
            "sender_user_code" => $user->user_code,
            "comment" => null,
            "file_code" => $file_code,
            "file_name" => $file_name,
            "comment_type" => $comment_type,
        ]);
        //ファイルデータ作成
        ChatFile::create([
            "chat_list_code" => $chat_code,
            "file_code" => $file_code,
            "file_path" => $path,
            "file_name" => $file_name,
        ]);
        $json = "成功";
        return response()->json($json);
    }

    //ファイルダウンロードAPI
    public function FileDownload(Request $request)
    {
        $file_code = $request->id;
        $file_data = ChatFile::where("file_code", $file_code)->first();

        $file_path = $file_data->file_path;

        $file_name = $file_data->file_name;

        $mime_type = Storage::mimeType($file_path);

        $headers = [['Content-Type' => $mime_type]];

        return Storage::download($file_path, $file_name, $headers);
    }
    //ID作成
    private function ID($length)
    {
        return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, $length);
    }
}
