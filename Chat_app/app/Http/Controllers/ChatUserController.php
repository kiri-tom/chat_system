<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChatUser;
use App\Oganization;
use App\OganizationUsers;
use Illuminate\Support\Facades\Hash;

class ChatUserController extends Controller
{
    //ユーザー作成画面
    public function create(){
        return view("user.create");
    }
    //ユーザー登録
    public function store(Request $request){
        $data = $request->all();
        $password = Hash::make($request->password);
        \Debugbar::info($data);
        $user = ChatUser::create([
            "user_name"=>$data["user_name"],
            "user_code"=>ChatUserController::ID(10),
            "email"=>$data["email"],
            "password"=>$password,
        ]);
        $oganization = Oganization::create([
            "oganization_name"=>$data["oganization_name"],
            "oganization_code"=>ChatUserController::ID(10),
        ]);
        OganizationUsers::create([
            "parent_id"=>$oganization->oganization_code,
            "child_id"=>$user->user_code,
        ]);
        return redirect(route("login"));

    }
    //ID作成
    private function ID($length){
        return substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, $length);
    }
}
