<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatFile extends Model
{
    protected $table = 'chat_file';
    protected $fillable = [
        'chat_list_code',
        'file_code',
        'file_path',
        'file_name',
    ];
}
