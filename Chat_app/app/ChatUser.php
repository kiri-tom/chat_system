<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class ChatUser extends Authenticatable
{
    //
    protected $table = 'chat_users';
    protected $fillable = ['user_code','email','password','user_name']; //保存したいカラム名が複数の場合
}
