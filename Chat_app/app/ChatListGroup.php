<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatListGroup extends Model
{
    protected $table = 'chat_list_group';
    protected $fillable = [
        'chat_list_code',
        'user_code',
        'group_name',
        'read_type',
    ];
}
