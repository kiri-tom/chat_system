<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ChatUser;

class ChatList extends Model
{
    protected $table = 'chat_list';
    protected $fillable = [
        'chat_list_code',
        'user_code',
    ];
    //ユーザーデータ取得
    public function User()
    {
        return $this->hasOne(ChatUser::class, 'user_code' , 'user_code');
    }
    //チャットデータ取得
    public function ChatData($chat_list_code)
    {
        $chat_data = Chat::where("chat_list_code",$chat_list_code)->get();
        return $chat_data;
    }
}
