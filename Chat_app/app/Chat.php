<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chat';
    protected $fillable = [
        'chat_list_code',
        'sender_user_code',
        'receiver_user_code',
        'comment',
        'file_code',
        'file_name',
        'comment_type',
        'read_type'
    ];
    //ユーザーデータ取得
    public function User()
    {
        return $this->hasOne(ChatUser::class, 'user_code' , 'user_code');
    }

    //ユーザーデータ取得
    public function User_data($user_code)
    {
        $user_data = ChatUser::where("user_code",$user_code)->first();
        return $user_data;
    }
}
